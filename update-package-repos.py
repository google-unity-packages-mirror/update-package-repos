import argparse
import fnmatch
import glob
import os
import re
import subprocess
import time
import urllib.error
import urllib.request


def parse_ver(verstr, check=False):
    """Parses "major.minor.patch" string into (major,minor,patch) tuple."""
    vermatch = re.match(r"^v?(\d+)\.?(\d*)\.?(\d*)$", verstr)
    if vermatch:
        ver = [int(vermatch[1])]
        if vermatch[2]:
            ver.append(int(vermatch[2]))
        if vermatch[3]:
            ver.append(int(vermatch[3]))
        return tuple(ver)
    if check:
        raise ValueError(f"Not a valid version format '{verstr}'")


def ver2str(ver):
    return ".".join([str(v) for v in ver])


def fetch_package_index() -> dict[str, set[tuple[int]]]:
    """Parses all .tgz links from Google Unity packages archive page.

    Returns a dict with each package name and set of (major,minor,patch) versions found.
    """
    packages = {}
    url = "https://developers.google.com/unity/archive"
    print(f'Fetching list of available Google Unity packages from "{url}"', flush=True)
    with urllib.request.urlopen(url) as response:
        for lineb in response:
            line = lineb.decode("utf-8", "backslashreplace")
            match = re.match(r"^.*https://dl\.google\.com/games/registry/unity/(.+)/.+-(.+?)\.tgz.*$", line)
            if match:
                name, verstr = match[1], match[2]
                ver = parse_ver(verstr)
                if ver:
                    pkgvers = packages.get(name) or set()
                    pkgvers.add(ver)
                    packages[name] = pkgvers
    return packages


# print all packages and versions
def print_pkg_index(pkg_index):
    for name, vers in pkg_index.items():
        print(name)
        vers = list(vers)
        vers.sort()
        for ver in vers:
            print(f"\t{ver}")


# Downloads and extracts package contents to working dir
def download_package(name, ver):
    filename = f"{name}-{ver2str(ver)}.tgz"
    url = f"https://dl.google.com/games/registry/unity/{name}/{filename}"

    retrytime = 10
    maxtries = 3
    tries = 0
    while True:
        print(f'Downloading "{url}"', flush=True)
        tries += 1
        try:
            urllib.request.urlretrieve(url, filename)
            break
        except urllib.error.ContentTooShortError as e:
            # The download seems to occasionally be interrupted without error. Rate limiting?
            print(e, flush=True)
            if tries < maxtries:
                print(f"Retrying in {retrytime*tries} seconds", flush=True)
                time.sleep(retrytime * tries)
            else:
                print(f"Failed after {tries} tries", flush=True)
                raise

    # strip-components=1 will export the content of the 'package' subfolder inside the archive
    subprocess.run(
        ["tar", "xzvf", filename, "--strip-components=1"],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
        check=True,
    )
    os.remove(filename)


def update_package_repo(
    name,
    versions: list[tuple[int]],
    project_namespace,
    access_token,
    authorname,
    authoremail,
    use_remote=True,
    single_push=False,
    domain="gitlab.com",
):
    """Downloads and commits specified versions of a google package to a repo.
    Then pushes the repo to gitlab under project_namespace/name using access_token.
    Checks the existing versions in the remote to update only new versions.

    Passed versions must be sorted with latest version last.

    Set use_remote to False to just create a repository locally.

    Set single_push to True to push all versions in one go instead of all separately.
    """
    print(f"\nUpdating package '{name}'", flush=True)

    if not os.path.exists(name):
        os.mkdir(name)
    wd = os.getcwd()
    os.chdir(name)

    existing_ver = None
    if use_remote:
        remote_path = f"{domain}/{project_namespace}/{name}"
        remote_url = f"https://gitlab-ci-token:{access_token}@{remote_path}.git"
        remote_web_url = f"https://{remote_path}"

        print(f'Repository "{remote_web_url}"', flush=True)

        existing_tags = []
        default_branch = None
        try:
            remotelist = subprocess.check_output(
                ["git", "ls-remote", "--symref", remote_url, "HEAD", "refs/tags/*"], text=True
            )

            # Using ls-remote to check existing tags as this allows to use fetch depth=1. Otherwise could use git describe after fetching
            for vertag in re.finditer(r"^.*\s+refs/tags/v?(\d+\.?\d*\.?\d*)$", remotelist, re.MULTILINE):
                existing_tags.append(parse_ver(vertag[1]))

            headmatch = re.match(r"^ref: refs/heads/(.*)\s+HEAD$", remotelist, re.MULTILINE)
            if headmatch:
                default_branch = headmatch[1]
        except subprocess.CalledProcessError:
            pass
        existing_tags.sort()

        if not default_branch:
            default_branch = "main"
            print(f"Default branch not found in remote. Using '{default_branch}'", flush=True)
        else:
            print(f"Default branch '{default_branch}'", flush=True)

        if existing_tags:
            existing_ver = existing_tags[-1]
    else:
        try:
            git_describe = subprocess.check_output(["git", "describe"], text=True)
            existing_ver = parse_ver(git_describe)
        except subprocess.CalledProcessError:
            pass

    last_ver = versions[-1]
    if existing_ver:
        if existing_ver >= last_ver:
            versions = []
        else:
            try:
                versions = versions[versions.index(existing_ver) + 1 :]
            except ValueError:
                pass

    if len(versions) == 0:
        print(f"Same or newer version is already in repository: '{existing_ver}' >= '{last_ver}'", flush=True)
    else:
        subprocess.run(["git", "init", "-b", "main"], check=False)
        subprocess.run(["git", "config", "user.name", authorname], check=True)
        subprocess.run(["git", "config", "user.email", authoremail], check=True)
        # suppress warning about line endings
        subprocess.run(["git", "config", "core.safecrlf", "false"], check=False)

        print(f"Using author '{authorname} <{authoremail}>'", flush=True)

        if use_remote:
            subprocess.run(["git", "remote", "add", "origin", remote_url], check=True)

            # For some reason git push hanged without this:
            # https://stackoverflow.com/questions/15843937/git-push-hangs-after-total-line
            subprocess.run(["git", "config", "http.postBuffer", "200M"], check=True)

            # fetch previous commit from remote to use as base
            subprocess.run(["git", "fetch", "--depth=1"], check=False)
            subprocess.run(["git", "checkout", default_branch], check=True)

            def push_to_remote():
                print(f'Pushing to "{remote_web_url}"', flush=True)
                subprocess.run(["git", "push", "--follow-tags"], check=True)

        print(f"New versions: {versions}", flush=True)

        # repeat download, commit and tag for all new versions of package
        for ver in versions:
            verstr = ver2str(ver)
            print(f"\nAdding version '{verstr}'", flush=True)

            # clean previous copy
            working_copy_files = glob.glob(".*") + glob.glob("*")
            try:
                working_copy_files.remove(".git")
            except ValueError:
                pass
            for file in working_copy_files:
                subprocess.run(["git", "rm", "-rqf", file], check=True)

            # download package copy for version
            download_package(name, ver)

            # parse changelog
            changelogstr = ""
            try:
                with open("CHANGELOG.md", "r") as changelogfile:
                    versionfound = False
                    for line in changelogfile:
                        # match first header containing a version number
                        # add all rows to changelog until next version header is encountered
                        # NOTE: requires at least 'major.minor' to reduce change of false positive
                        # FIXME: assumes no other content comes after the last version entry
                        if re.match(r"^\s*#+\s+(v|ver|version)?\s*v?\d+\.\d+\.?\d*.*$", line, re.IGNORECASE):
                            if not versionfound:
                                versionfound = True
                            else:
                                break
                        if versionfound:
                            changelogstr += line
            except IOError:
                pass

            # print(changelogstr, flush=True)

            # Enable LFS for some large files to avoid upload limits
            subprocess.run(["git", "lfs", "track", "*.so"], check=True)
            subprocess.run(["git", "lfs", "track", "*.bundle"], check=True)

            # commit and tag
            subprocess.run(["git", "add", "-A"], check=True)
            subprocess.run(
                ["git", "commit", "-m", f"{name} {verstr}\n\n{changelogstr}", "--cleanup=whitespace"], check=True
            )
            # using -f in case local copy contains the tag already
            subprocess.run(
                ["git", "tag", "-a", "-f", verstr, "-m", f"Version {verstr}\n\n{changelogstr}", "--cleanup=whitespace"],
                check=True,
            )

            if use_remote and not single_push:
                push_to_remote()

        if use_remote and single_push:
            push_to_remote()

    os.chdir(wd)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--project-namespace")
    parser.add_argument("--access-token")
    parser.add_argument("--author-name")
    parser.add_argument("--author-email")
    parser.add_argument("--local", action="store_true")
    parser.add_argument("--single-push", action="store_true")
    parser.add_argument("package_names", nargs="*")

    args = parser.parse_args()

    pushargs = (
        args.project_namespace,
        args.access_token,
        args.author_name,
        args.author_email,
        not args.local,
        args.single_push,
    )

    wildcards = []
    for pkgname in args.package_names:
        parts = pkgname.split(":")
        name = parts[0]
        if len(parts) > 1:
            ver = parts[1]
            if ver[0] in ("<", ">"):
                # e.g. '>1.2.168'
                ver = (ver[0], parse_ver(ver[1:], check=True))
            else:
                ver = (None, parse_ver(ver, check=True))
        else:
            ver = "*"
        wildcards.append((name, ver))

    pkg_index = fetch_package_index()

    # print_pkg_index(pkg_index)

    index = pkg_index.items()
    packages = {}
    if wildcards:
        for wcname, wcver in wildcards:
            found = False
            for name, versions in index:
                if fnmatch.fnmatch(name, wcname):
                    vers = list(versions)
                    vers.sort()
                    if wcver != "*":
                        if wcver[0] == ">":
                            vers = vers[vers.index(wcver[1]) :]
                        elif wcver[0] == "<":
                            vers = vers[: vers.index(wcver[1])]
                        else:
                            vers = [wcver[1]]
                    packages[name] = vers
                    found = True
            if not found:
                print(f"No packages found matching '{wcname}:{wcver}'", flush=True)
    else:
        for name, versions in index:
            vers = list(versions)
            vers.sort()
            packages[name] = vers

    for name, versions in packages.items():
        update_package_repo(name, versions, *pushargs)


if __name__ == "__main__":
    main()
